<?php
require __DIR__ . '/vendor/autoload.php';

use Curl\Curl;
use Curl\MultiCurl;

/**
 * @throws ErrorException
 */
function multirequest($endpoints) {
    $multi_curl = new MultiCurl();

    $start = microtime(true);

    $multi_curl->success(function($instance) use ($multi_curl) {
        echo 'call to "' . $instance->url . '" was successful.' . "\n";
        echo 'response:' . "\n";
        print_r($instance->response);
        $multi_curl->stop();
    });

    $multi_curl->error(function($instance) {
        echo 'call to "' . $instance->url . '" was unsuccessful.' . "\n";
        echo 'error code: ' . $instance->errorCode . "\n";
        echo 'error message: ' . $instance->errorMessage . "\n";
    });

    $multi_curl->complete(function() use ($start) {
        echo 'call completed' . "\n";
        echo 'script execution time: ' . round(microtime(true) - $start, 4) . ' sec' . PHP_EOL;
    });

    foreach ($endpoints as $ep) {
        $multi_curl->addCurl(new Curl($ep[CURLOPT_URL], $ep));
    }

    $multi_curl->start();
}

$data = json_encode([['document_type'  => 'ctc', 'document_value' => 9940368866]], JSON_UNESCAPED_UNICODE);

$endpoints = [
    [
        CURLOPT_URL => 'https://api.xn--80ajbekothchmme5j.xn--p1ai/gis/moneta?XDEBUG_SESSION_START=PHPSTORM&priority=8&accrualTypeId=gibdd',
        CURLOPT_POST => 1,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Google-Captcha-Token: m53YUELHkejMjLEzQ_SXDYLB-LM_cirt',
            'Cookie: _csrf-frontend=9af9dca865c71da1dbfb4258c97d51c6e9dee5881f7fe39a78abe6fe6f366483a%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%22nt-f2czH6TDupMEaZLVCK9-FChclqQKU%22%3B%7D',
            'Authorization: Bearer ItgDjzHIWDxU1a0QTLKHnXT4nO12T0XJ'
        ],
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_TIMEOUT => 30
    ],
    [
        CURLOPT_URL => 'https://api.xn--80ajbekothchmme5j.xn--p1ai/gis/a3?XDEBUG_SESSION_START=PHPSTORM&accrualTypeId=gibdd&priority=8',
        CURLOPT_POST => 0,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Google-Captcha-Token: m53YUELHkejMjLEzQ_SXDYLB-LM_cirt',
            'Cookie: _csrf-frontend=9af9dca865c71da1dbfb4258c97d51c6e9dee5881f7fe39a78abe6fe6f366483a%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%22nt-f2czH6TDupMEaZLVCK9-FChclqQKU%22%3B%7D'
        ],
        CURLOPT_POSTFIELDS => $data,
    ],
    [
        CURLOPT_URL => 'https://www.bing.com/search?q=hello+world',
    ]
];

multirequest($endpoints);